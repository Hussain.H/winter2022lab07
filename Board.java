public class Board{
	private Square[][] tictactoeBoard;
	
	//Constructor
	public Board(){
		tictactoeBoard = new Square[3][3];
		for(int i = 0;i<tictactoeBoard.length;i++){
			for(int j=0;j<tictactoeBoard[i].length;j++){
				tictactoeBoard[i][j]=Square.BLANK;
			}
		}
		
	}
	
	//Double check this after
	
	public String toString(){
		String result="";
		for(int i = 0;i<tictactoeBoard.length;i++){
			for(int j=0;j<tictactoeBoard[i].length;j++){
				result +=tictactoeBoard[i][j]+" ";
	}
	result+="\n";
	
	}
	return result;
}
	public boolean placeToken(int row, int col,Square playerToken){
		if(row <0 || row >= tictactoeBoard.length || col<0 || col>= tictactoeBoard[row].length){
			return false;
	}
		if(tictactoeBoard[row][col]==Square.BLANK){
			tictactoeBoard[row][col] = playerToken;
			return true;
		}
		return false;
}

	public boolean checkIfFull(){
		for(int i=0;i<tictactoeBoard.length;i++){
			for(int j =0;j<tictactoeBoard[i].length;j++){
				if(tictactoeBoard[i][j]==Square.BLANK){
					return false;
				}
			}
	}
	return true;

}

	private boolean checkIfWinningHorizontal(Square playerToken){
			for(int i =0;i<3;i++){
				if(tictactoeBoard[i][0]== playerToken && tictactoeBoard[i][1] == playerToken && tictactoeBoard[i][2]== playerToken){
					return true;
				}
			}
			return false;
	}
	
	private boolean checkIfWinningVertical(Square playerToken){
			for(int j =0;j<3;j++){
				if(tictactoeBoard[0][j]== playerToken && tictactoeBoard[1][j] == playerToken && tictactoeBoard[2][j]== playerToken){
					return true;
				}
			}
			return false;
	}

		public boolean checkIfWinning(Square playerToken){
			if(checkIfWinningHorizontal(playerToken) || checkIfWinningVertical(playerToken)){
				return true;
			}
			return false;
		}


}

