import java.util.Scanner;

public class TicTacToeGame{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
	System.out.println("Welcome to Tic Tac Toe! :)");
	System.out.println("Player 1 will be X");
	System.out.println("Player 2 will be O");
	
	Board board = new Board();
	boolean gameOver = false;
	int player =1;
	Square playerToken = Square.X;

	while(!gameOver){
		System.out.println(board);
		
		if(player ==1){
			playerToken= Square.X;
		}
		else{
			playerToken=Square.O;
		}
		int col =0;
		int row =0;
		boolean validinput = false;
	do {
		System.out.println("Player "+player+ " Please enter the row and column you want to occupy (1-3)" );
		row = sc.nextInt() - 1;
		col = sc.nextInt() -1;
	} while(!board.placeToken(row, col, playerToken));
	
		if(board.checkIfFull()){
			System.out.println(board);
			System.out.println("It's a tie!");
			gameOver=true;
		}
		else if (board.checkIfWinning(playerToken)){
			System.out.println(board);
			System.out.println("Player "+ player+ " Wins!");
			gameOver=true;
		}
		else{
		player++;
			if(player > 2){
			player =1;	
			}
		}
	}

}
}